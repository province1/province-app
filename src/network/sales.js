import { api_handler } from './index'
import axios from 'axios'




export default class SaleNetworkRequest {
    

    #data_source_urls = {
        
        public_data_baseURL: process.env.VUE_APP_PUBLIC_DATA_URL,
        sales_url: process.env.VUE_APP_SALES_URL
        
    }
    
    
    #sales_url_directory = {
        
        sales: 'sales',
        bids_read: 'api/bids/read',
        leading_bid: 'api/bids/leading'
        
    }
    
    
    #axios_instance(url_lookup){
        return axios.create({baseURL: this.#data_source_urls[url_lookup]});
    }
    
    async #bid_data(sale_id, url_key){
        
        const data = {sale_id}
        const sales_service = this.#axios_instance('sales_url')
        var result =  await api_handler.post_request( data, sales_service, this.#sales_url_directory[url_key] );
         
        if(result.error)throw("Error getting sales data")
        
        return result
        
    }
    

    async get_all_sales(){
        
        const db_service = this.#axios_instance('public_data_baseURL');
        var result =  await api_handler.get_request( db_service, this.#sales_url_directory['sales'] );
        if(result.error)throw("Error getting sales data");

        return result.result


    }
    
    
    async get_bids(sale_id){
        
        return await this.#bid_data(sale_id, 'bids_read')
        
    }
    
    async get_leading_bid(sale_id){
        
        return await this.#bid_data(sale_id, 'leading_bid')

        
    }
    

    
    
    
}




export { SaleNetworkRequest };