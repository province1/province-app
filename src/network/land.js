import { api_handler } from './index'
import axios from 'axios'


export default class LandNetworkRequest {
    
    public_data_baseURL = process.env.VUE_APP_PUBLIC_DATA_URL;
    BULLDOZER_URL=process.env.VUE_APP_BULLDOZER_URL
    
    async get_owned_land(data){
        
        const url = 'land/ownerquery'

        const db_service = axios.create({baseURL: this.public_data_baseURL})

        var result =  await api_handler.post_request( data, db_service, url )
        

        return result


    }
    
    async clear_land(data){
        
        
        
        const url = "api/equipment-ops/deploy/bulldozer/1"
        

        const bulldozer_service = axios.create({baseURL: this.BULLDOZER_URL})

        return  await api_handler.post_request( data, bulldozer_service, url)


    }
    

    
    
    
}




export { LandNetworkRequest };
