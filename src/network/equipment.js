
import { api_handler } from './index'
import axios from 'axios'


export default class EquipmentNetworkRequest {
    
    public_data_baseURL = process.env.VUE_APP_PUBLIC_DATA_URL;
    BULLDOZER_URL=process.env.VUE_APP_BULLDOZER_URL;
    bulldozer_url_legacy = process.env.VUE_APP_BULLDOZER_URL_LEGACY
    
    async get_owned_bulldozers(query){
        
        const url = 'equipment/controlquery'
        const db_service = axios.create({baseURL: this.public_data_baseURL})

        var result = await api_handler.post_request( query, db_service, url )
        return result.result

    }
    
    async clear_land(serial, data){
        
        
        const url = "/equipment-ops/deploy/bulldozer/"+serial
        const bulldozer_service = axios.create({baseURL: this.BULLDOZER_URL})

        try{
            var result =  await api_handler.post_request( data, bulldozer_service, url)
        } catch(err){
            
             console.log("Equipment Error", err)
        }
       
        return result

    }
    
    async get_clearing_progress(serial){
        
        const url = "/clearing_status";
        const bulldozer_service_legacy = axios.create({baseURL: this.bulldozer_url_legacy})
        
        try {
            var result = await api_handler.post_request({serial}, bulldozer_service_legacy, url)
        } catch(err){
            
             console.log("Equipment Error", err)
        }
        return result
        
        
    }
    
    

    
    
    
}




export { EquipmentNetworkRequest };
