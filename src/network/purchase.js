
import { api_handler } from './index'
import axios from 'axios'


export default class PurchaseNetworkRequest {
    
    public_data_baseURL = process.env.VUE_APP_PUBLIC_DATA_URL;
    SALES_URL=process.env.VUE_APP_SALES_URL
    
    
    //ToDo: Review whether network_handler is needed or not
    async bid_land(data, network_handler, network_notification, success_notification, error_handler){
        
        network_notification(true)
        
        const url = "/api/bids/create"
        
        console.log("baseURl", this.SALES_URL)
        

        const sales_service = axios.create({baseURL: this.SALES_URL})

        try{
            await api_handler.post_request( data, sales_service, url);
            success_notification()
        } catch(err){
            error_handler(err.message)

        } finally{
            
            network_notification(false)
            
        }

    }
    
    
    async buy_land(data, network_handler, network_notification, success_notification, error_handler){
        
        network_notification(true)
        
        const url = "/api/sales/buy"
        
        console.log("baseURl", this.SALES_URL)
        

        const sales_service = axios.create({baseURL: this.SALES_URL})

        try{
            await api_handler.post_request( data, sales_service, url);
            success_notification()
        } catch(err){
            error_handler(err.message)

        } finally{
            
            network_notification(false)
            
        }

    }
    

    
    
    
}




export { PurchaseNetworkRequest };
