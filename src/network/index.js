



class api_handler {
    
    
    
    static async get_request( db_service, url ){
        
        let result
        
        try{
            
            result = await db_service.get(url);
            return result.data
            
            
        } catch(err) {
            
            // ToDo: Response should return an error object with atleast a message and code

            console.log("GET Request Error", err)
            console.log("GET Request Error",  err.response.data, err.response.data.error_code);
            

        }
    }
    
    
    static async post_request( data, service, url){
    
        let result
    

        try{
            
           
            result = await service.post(url, data);
            
            return result.data
            
        
            
        } catch(err) {
            
            // ToDo: Response should return an error object with atleast a message and code
            
            
            console.log("Post Request Error", err);
            console.log("Post Request Error",  err.response.data, err.response.data.error_code);
            throw(err.response.data);
            
            
            
        }
        
    
    }
    
    async delete_request( network_request ){
    
        console.log(network_request)
    
    }
    
    
    
}


export { api_handler }