import { api_handler } from './index'
import axios from 'axios'


export default class MaterialsNetwork {
    
    public_data_baseURL = process.env.VUE_APP_PUBLIC_DATA_URL;
    
    async get_material( address, type_code ){

        const data = {address, type_code}
        const url = '/materials/balance';
         
        const db_service = axios.create({baseURL: this.public_data_baseURL})

        var result =  await api_handler.post_request( data, db_service, url )
        
        return result
        
    }
    
    
    
}

