import { createRouter, createWebHistory } from 'vue-router';

import ViewEquipment from '../views/Equipment';
import ViewLand from '../views/Land';
import ViewSales from '../views/Sales';
import ViewMap from '../views/Map';
import ViewMaterial from '../views/Material';


const routes = [
    
    {
        path: '/',
        name: 'Main',
        component: ViewSales,
    },
    {
        path: '/map',
        name: 'Map',
        component: ViewMap,
    },
    {
        path: '/equipment',
        name: 'Equipment',
        component: ViewEquipment,
    },
    {
        path: '/land',
        name: 'Land',
        component: ViewLand,
    },
    {
        path: '/sales',
        name: 'Sales',
        component: ViewSales,
    },
    {
        path: '/material',
        name: 'Material',
        component: ViewMaterial,
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;
