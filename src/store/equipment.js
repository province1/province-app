
import { EquipmentNetworkRequest } from '../network/equipment.js'


const state = () => ({
  
  bulldozer_items: []

})

// getters
const getters = {
    bulldozer_items: state => state.bulldozer_items,
}

// actions
const actions = {
  
  
  async refresh_bulldozer_list ({ commit }, address ) {
        
        
        

        //var data = data.address

        const equipment_network_request = new EquipmentNetworkRequest();
        
        const bulldozer_list = await equipment_network_request.get_owned_bulldozers({address })
        
        /*
          ! Important! Date here is assumed to be cases where the address is either the owner or an active lease holder
          Do this here to reduce the requirement for multiple UI components
        */
        const results_lease_flag = bulldozer_list.map( x => {
            
            
            if(x.lessee == address){ 
                x.lease = true
                x.control_status = "Lease"
                
            }
            
            if(x.owner == address){
              x.lease = false
              x.control_status = "Owner"
            }
            
            
            
            return x
        })
        
        commit('set_bulldozer_items', results_lease_flag)
        
  }
  
  
}

// mutations
const mutations = {
  
  set_bulldozer_items (state, data){
        
        state.bulldozer_items = data
        
    }
  
  
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}