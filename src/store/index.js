import { createStore } from 'vuex'

import wallet from './wallet'
import equipment from './equipment'
import status from './status'
import land from './land'
import sales from './sales'
import materials from './materials'


export default createStore({

  modules: {
    wallet,
    equipment,
    status,
    land,
    sales,
    materials
  }
})

