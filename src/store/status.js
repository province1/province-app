const state = () => ({
  
  waiting_signature: false,
  success_indicator: false,
  waiting_network: false,
  error_indicator: false,
  error_message: null,
  reset: false

})

// getters
const getters = {
  
  waiting_signature: state => state.waiting_signature,
  success_indicator: state => state.success_indicator,
  waiting_network: state => state.waiting_network,
  error_indicator: state => state.error_indicator,
  error_message: state => state.error_message,
  reset: state => state.reset
  

}

// actions
const actions = {
    
    change_waiting_signature ({ commit }, new_value) {
        
        commit('mutate_waiting_signature', new_value)
        
    },
    
    change_success_indicator({ commit }, new_value){
      
      commit('mutate_success_indicator', new_value)
      
    },
    

    change_waiting_network ({ commit }, new_value) {
        
        commit('mutate_waiting_network', new_value)
        
    },
    
    change_error_indicator ({ commit }, new_value) {
        
        commit('mutate_error_indicator', new_value)
        
    },
    
    change_error_message ({ commit }, new_value) {
        
        commit('mutate_error_message', new_value)
        
    },
    
    async flip_reset ({ commit }) {
        await commit('mutate_reset', true)
        /*await*/ commit('mutate_reset', false)
        
    },
    
    // async not needed here. eventual consistency is OK.
    reset_indicators ({ commit }) {
        
        
        commit('mutate_waiting_signature', false);
        commit('mutate_success_indicator', false);
        commit('mutate_waiting_network', false);
        commit('mutate_error_indicator', false)
        commit('mutate_error_message', null)
        
    }
    
    
    
    
    
}

// mutations
const mutations = {
    
    mutate_waiting_signature (state, new_status){
        
        state.waiting_signature = new_status
        
    },
    
    mutate_success_indicator (state, new_status){
        
        state.success_indicator = new_status
        
    },
    
    mutate_waiting_network (state, new_status){
        
        state.waiting_network = new_status
        
    },
    mutate_error_indicator (state, new_status){
        
        state.error_indicator = new_status
        
    },
    mutate_error_message (state, new_status){
        
        state.error_message = new_status
        
    },
    mutate_reset(state, new_status){
        
        state.reset = new_status
        
    },
    
    
    
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}