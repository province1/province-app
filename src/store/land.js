
import { LandNetworkRequest } from '../network/land.js'


const state = () => ({
  
  land_items_forest: [],
  land_owned_all: []

})

// getters
const getters = {
    land_items_forest: state => state.land_items_forest,
    land_owned_all: state => state.land_owned_all
}

// actions
const actions = {
  
  
  async refresh_owned_land ({ commit }, address){
    
    const land_network_request = new LandNetworkRequest();
    
    
    //ToDo: Setup UI for and handle network errors
    const land_list = await land_network_request.get_owned_land({address})
    
    commit('set_land_all_owned', land_list.result)
    
  },
  
  
  async refresh_land_list_forest ({ commit }, address ) {
        
        
    const land_network_request = new LandNetworkRequest();
    
    //ToDo: Setup UI for and handle network errors
    const land_list = await land_network_request.get_owned_land({address})
    
    var land_list_forest=[]
    
    if(land_list.result.length > 0){
      land_list_forest = land_list.result.filter(x => {
        return x.land_status=="forest"
      })
    }
    
    commit('set_land_items_forest', land_list_forest)
        
  }
  
  
}

// mutations
const mutations = {
  
  set_land_items_forest (state, data){
        
        state.land_items_forest = data
        
  },
  
   set_land_all_owned (state, data){
        
        state.land_owned_all = data
        
    }
  
  
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}