
import { SaleNetworkRequest } from '../network/sales.js'



const state = () => ({
  
  sales: []

})

// getters
const getters = {
    sales: state => state.sales,
}

// actions
const actions = {
  
  
  async refresh_sales_list ({ commit } ) {
        

    const sales_network_request = new SaleNetworkRequest();
    
    const sales_list = await sales_network_request.get_all_sales();
    
    const sales_list_bids = await Promise.all(sales_list.map( async x => {
      
      if(x.pricing_model == 'auction'){
        
        const bids = await sales_network_request.get_bids(x.sale_id)
        x.bids = bids
        
      }

      return x
      
    }))
    
    const sales_list_leading = await Promise.all(sales_list_bids.map( async x => {
      
      if(x.pricing_model != "auction"){return x}
      
      const leading = await sales_network_request.get_leading_bid(x.sale_id);
      
      x.leading_bid = leading.leading_bid
      return x
      
    }))
    

    commit('set_sale_items', sales_list_leading);
        
  }
  
  
}

// mutations
const mutations = {
  
  set_sale_items (state, data){
        
        state.sales = data
        
    }
  
  
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}