
import util from '@/utils/web3_utils'


var err_msg = ""
var error_data = {}


const state = () => ({
  
  mpl_token_balance: 5,
  mpl_token_allowance: 5,
  wallet_address: '0x0000000000000000000000000000000000000000',
  wallet_error_message: {error: false, error_msg: ""},
  approval_result: {error: false},
  wallet_status: {connected_status: false},
  metamask_install_status: false

  
})

// getters
const getters = {
  
  mpl_token_balance: state => state.mpl_token_balance,
  mpl_token_allowance: state => state.mpl_token_allowance,
  wallet_address: state => state.wallet_address,
  wallet_status: state => state.wallet_status,
  wallet_error_message: state => state.wallet_error_message,
  approval_result: state => state.approval_result,
  metamask_install_status: state => state.metamask_install_status

  
}

// actions
const actions = {
  
  check_metamask({ commit }){
      var status =  util.check_metamask_install()
      
      console.log("Metamask Install Status: ", status)
      
      commit('update_metamask_install_status', status)
      
    },
    
    connect_metamask({ commit }){
      
      
      return new Promise( (resolve, reject) =>{
        
        util.request_metamask_account().then( (result) => {
        
        
        if(result.error === false){
          
          var final_data = {connected_status: true, account: result.eth_request_result[0], chain_id: result.chain_id}
          
          
          
          if(result.chain_id != process.env.VUE_APP_NETWORK_ID) {
          
            err_msg = "Please make sure you are connected to the avalanche mainnet"
            error_data = {error: true, err_msg}
            
            commit('update_metamask_account', {connected_status: false})
            
            commit('publish_error', error_data)
            reject(error_data)
            
          }else {
            
            commit('update_metamask_account', final_data)
            final_data.error = false
            
            error_data = {error: false, err_msg: ""}
            
            commit('publish_error', error_data)
            
            resolve(final_data)

            
          }
          
          
        } else {
      
          err_msg = "You did not approve the metamask connection. Please try again or get help in the Discord server."
          error_data = {error: true, err_msg}
          
          commit('publish_error', error_data)
          reject(error_data)
        }
        
      })
        
        
      })
      
      
      
      //util.check_provider()
      
    },
    
    
    
    async refresh_mpl_balance({ commit }){
      const balance = await util.get_mpl_balance();
      
      commit('set_mpl_balance', balance)
      
      
      
    }


  
  
  
}

// mutations
const mutations = {
  
  update_metamask_install_status (state, data){
        
         
      state.metamask_install_status = data
         
    },
    
  update_metamask_account (state, data) {
    
    state.wallet_status = {connected_status: data.connected_status, chain_id: data.chain_id}
    state.wallet_address = data.account;
    
  },
  
  set_mpl_balance (state, data) {
    
    state.mpl_token_balance = data
    
    
  },
    
    
  publish_error(state, data){
    
    console.log("publish error data: ", data)
    
    state.wallet_error_message = data
    
  },
    
  update_approval(state, data){
    
    state.approval_result = data
    
  },

  
  
  
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}