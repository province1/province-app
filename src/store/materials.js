import MaterialsNetwork from '@/network/materials'


const state = () => ({
  
  timber: [],

})

// getters
const getters = {
    timber: state => state.timber
}

// actions
const actions = {
  
  
  async get_timber ({ commit }, address){
    
    const network_request = new MaterialsNetwork();
        
    const timber_balance = await network_request.get_material(address, 0)
    
    console.log(222, timber_balance)
    
    commit('set_timber', timber_balance)
    
  },
  

  
  
}

// mutations
const mutations = {
  

   set_timber (state, data){
        
        state.timber = data
        
    }
  
  
}

export default {
//namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    
  }
}