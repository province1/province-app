var ethereum = window.ethereum;
import { ethers } from 'ethers';
// import { Buffer } from 'buffer/'
const eth_provider = new ethers.providers.Web3Provider(window.ethereum);
import MapleContract from "./MapleContract.json";

const env_type = process.env.VUE_APP_CONTRACT_KEY

const mpl_contract_address = MapleContract[env_type]

import store from '../store'


function check_metamask_install(){
    
    
    if (typeof ethereum !== 'undefined') {
        return true
    } else {
        return false
    }
    
}

async function request_metamask_account(){
    
    
    
    
    try{
        
        var eth_request_result = await ethereum.request({ method: 'eth_requestAccounts' });
        
        const chain_id_hex = await ethereum.request({ method: 'eth_chainId' });
        const chain_id_number = parseInt(chain_id_hex, 16)

        return {error: false, error_code: 0, eth_request_result, chain_id: chain_id_number}
        
      } catch(e){
        
        if(e.code == 4001){
          
          console.log("eth_requestAccounts error: ", "User rejected request")
          return {error: true, error_code: 4001, error_message: "You rejected the MetaMask request"}
          
        } else {
          
          console.log("unhandled eth_requestAccounts error: ", e)
          return {error: true, error_code: 1000, error_message: "Unknown error with MetaMask. Get help in Discord."}
          
          
        }
        
      }
      

}


async function generate_signed_msg(address, msg_json, signature_notifications, signature_error_handler){
    

    // Random string
    msg_json.random_string = (Math.random()).toString(36).substring(2);
    
    const msg_to_sign = JSON.stringify(msg_json);
    
    signature_notifications(true);
    
    try {
      const sign = await ethereum.request({
        method: 'personal_sign',
        params: [msg_to_sign, address, 'Example password'],
      });
      
      signature_notifications(false);
      return {error: false, signature: sign, error_msg: null, message: msg_to_sign}
    
      
    } catch (err) {
      
      signature_notifications(false);
      signature_error_handler(err)
      
    }
    
}


async function get_mpl_balance(){
    
    
    const contract = new ethers.Contract(mpl_contract_address, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
    
    const address = store.getters.wallet_address;
    
    const balance = await contract.balanceOf(address);
    
    
    return ethers.utils.formatEther(balance)
                                            
}

async function get_mpl_allowance(address){
    
    
    var contract = new ethers.Contract(mpl_contract_address, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
                                            
    var allowance = await contract.allowance(address, mpl_contract_address)
    
    
    return ethers.utils.formatEther(allowance)
                                            
}

function contract_factory(){
    
    const contract = new ethers.Contract(mpl_contract_address, 
                                            MapleContract.abi,
                                            eth_provider.getSigner())
                                            
    return contract
    
    
}

//ToDo: This process needs to be modified such that there is a check on the current allowance meets all obligations rather than performing an allowance increase and then waiting for the onchain event.
const approval_confirmation = (address, treasury_address, network_notification) => {
  return new Promise ( (resolve ) => {
    
    network_notification(true);
    const contract = contract_factory()
      
    const filterTo = contract.filters.Approval(
        address,
        treasury_address
    );
    
    contract.once(filterTo, async (from, to, amount, event) => {
    
        if(event.event == "Approval"){
          network_notification(false)
          resolve(true);
        }
        
        
    });

  })
}


// ToDo: contract factory should be passed in as a parameter to make testing easier
const increaseAllowance = async (treasury, allowance, signature_notifications, signature_error_handler) => {
  
  let result;
  signature_notifications(true);
  
  try {
    const contract = contract_factory();
    result = await contract.increaseAllowance(treasury, ethers.utils.parseEther(allowance));
    signature_notifications(false);
    return result
    
  } catch(err) {
    signature_notifications(false);
    signature_error_handler(err)
    
  }
}




export default { generate_signed_msg, check_metamask_install, request_metamask_account, get_mpl_balance, get_mpl_allowance, increaseAllowance, contract_factory, approval_confirmation }