import store from '../store'



const bulldozer_network_request_handler= (response)=>{
    
    if(response.data.error == false) return true
    else return false
    
}

export class Bulldozer{
    
    constructor(serial, status){
        this.serial = serial;
        this.status = status;
        this.wallet_address = store.getters.wallet_address;
    }
    
    
    
}

export class IdleBulldozer extends Bulldozer {
    
        
    constructor(serial){
        super(serial, "idle")
    }
    
    
    signature_notifications( new_value ){
        store.dispatch('change_waiting_signature', new_value)
    }
    
    network_notification( new_value ){
        store.dispatch('change_waiting_network', new_value)
    }
    
    success_notification(){
        store.dispatch('change_success_indicator', true)
    }
    
    network_error_hanlder(message=null){
        store.dispatch('change_error_indicator', true);
        
        if(message != null){
            store.dispatch('change_error_message', message);
        } else {
            store.dispatch('change_error_message', "Network Error. Get Help in Discord");
        }
        
    }
    
    async deploy(plot_int,  web3_signer, network_request){
        
        
        const msg_json = {
            
            plot_int,
            sender_address:this.wallet_address,
            timestamp: Date.now()
            
        }
        

        store.dispatch('change_waiting_signature', true);
        
        // create signature
        var signature_result = await web3_signer(this.wallet_address, msg_json, this.signature_notifications, this.signature_error_handler);

        var request_data = {msg: signature_result.message, signature: signature_result.signature}
        
        store.dispatch('change_waiting_signature', false);

        store.dispatch('change_waiting_network', true);
        
        //ToDo: Add Error Handling
        await network_request(this.serial, request_data, bulldozer_network_request_handler,this.network_notification, this.success_notification, this.network_error_hanlder);

        store.dispatch('change_waiting_network', false);
        

        store.dispatch('change_success_indicator', true);

        
    }
    
}
