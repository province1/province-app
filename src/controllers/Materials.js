import store from '../store'
import MaterialsNetwork from '@/network/'

export default class Materials {
    
    
    constructor(){
        
        this.address = store.getters.wallet_address;
        
    }
    
    async get_timber(){
        
        const network_request = new MaterialsNetwork();
        
        const timber_balance = network_request.get_material(this.address, 0)
        
        return timber_balance
        
        
    }
    
    
    
    
    
    
}