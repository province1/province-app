import store from '../store'



// Offer encompasess both sales and leases



export default class Offer {
    

    constructor( start, end_days, pricing_model, item_type, sale_type, seller, lease_length = null){
        
        
        // Start needs to be calculated in the view / component because it is dependent on whether the user selected NOW or not.
        
        const end =  end_days * 1000*60*60*24 + start;
        this.pricing_model = pricing_model.toLowerCase();
        this.item_type = item_type;
        
        this.offer_data = {
            
            start, 
            end,
            item_type, 
            sale_type, 
            seller,
            random_string: Math.floor(100000 + Math.random() * 900000),
            pricing_model,
            timestamp: Date.now()
            
        };
        
        if (sale_type == "lease") this.offer_data.lease_length = lease_length;
        
        this.wallet_address = store.getters.wallet_address;
        
    }
    
    signature_notifications( new_value ){
        store.dispatch('change_waiting_signature', new_value)
    }
    
    network_error_hanlder(message=null){
        store.dispatch('change_error_indicator', true);
        
        if(message != null){
            store.dispatch('change_error_message', message);
        } else {
            store.dispatch('change_error_message', "Network Error. Get Help in Discord");
        }
        
    }
    network_notification( new_value ){
        store.dispatch('change_waiting_network', new_value)
    }
    
    success_notification(){
        store.dispatch('change_success_indicator', true)
    }
    
    signature_error_handler(err){
        
        store.dispatch('change_error_indicator', true);
        
        if(err.code == 4001 || err.code == 1000) {
            store.dispatch('change_error_message', err.message);
            throw(err.message)
        } else {
            
            store.dispatch('change_error_message', "Unknown Signature Error");
            throw("Unknown Signature Error")
            
        }
        
        
                
    }
    
    offer_network_request_handler(response){
    
        if(response.data.error == false) return true
            else return false
    
    }

    
    

    async create_sale_land (id, web3_signer, network_request, list_price = null) {
    
        var offer_data = this.offer_data

        offer_data.sale_type = this.offer_data.sale_type;
        
        if(this.offer_data.item_type == "equipment"){
            offer_data.serial = id
        } else {
            offer_data.plot_int = id;
        }
        
        

        if(offer_data.pricing_model == "fixed-price"){
          offer_data.price =  list_price
        }
        
        var signature_result = await web3_signer(this.wallet_address, offer_data, this.signature_notifications, this.signature_error_handler);

        var request_data = {msg: signature_result.message, hash: signature_result.signature}
        

        await network_request(request_data, this.network_notification, this.success_notification, this.network_error_hanlder);
        

    }
        

    
}