import store from '../store'





const purchase_network_request_handler= (response)=>{
    
    if(response.data.error == false) return true
    else return false
    
}


export default class NewBid {

    constructor( sale_id, bid_amount, address ){
        
        this.sale_id = sale_id;
        this.bid_amount = bid_amount;
        this.address = address;
        this.treasury_address = process.env.VUE_APP_TREASURY_ADDRESS

    }
    
    
    signature_notifications( new_value ){
        store.dispatch('change_waiting_signature', new_value)
    }
    
    network_notification( new_value ){
        store.dispatch('change_waiting_network', new_value)
    }
    
    success_notification(){
        store.dispatch('change_success_indicator', true)
    }
    
    network_error_hanlder(message=null){
        store.dispatch('change_error_indicator', true);
        
        if(message != null){
            store.dispatch('change_error_message', message);
        } else {
            store.dispatch('change_error_message', "Network Error. Get Help in Discord");
        }
        
    }
    
    
    signature_error_handler(err){
        
        store.dispatch('change_error_indicator', true);
        
        if(err.code == 4001 || err.code == 1000) {
            store.dispatch('change_error_message', err.message);
            throw(err.message)
        }
        
        store.dispatch('change_error_message', "Unknown Signature Error");
        throw("Unknown Signature Error")
                
    }
    
    async increase_allowance( allowance_increaser, approval_confirmation ){
        
        await allowance_increaser(this.treasury_address, String(this.bid_amount), this.signature_notifications, this.signature_error_handler);

        //ToDo: This process needs to be modified such that there is a check on the current allowance meets all obligations rather than performing an allowance increase and then waiting for the onchain event.
        await approval_confirmation(this.address, this.treasury_address, this.network_notification);
        
    }
    
    
    async create_bid (web3_signer, network_request) {
    
        const msg_json = {
            sale_id: this.sale_id,
            bid: this.bid_amount,
            address: this.address,
            random_string: Math.floor(Math.random() * 1000000000000),
        }



        const signature_result = await web3_signer(this.address, msg_json, this.signature_notifications, this.signature_error_handler);
        
        const request_data = {msg: signature_result.message, hash: signature_result.signature}

        await network_request(request_data, purchase_network_request_handler, this.network_notification, this.success_notification, this.network_error_hanlder);
       
        
        
     
    }
        

    
    
}